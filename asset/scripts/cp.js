function HTMLEncode(html) {
  var temp = document.createElement("div");
  (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
  var output = temp.innerHTML;
  temp = null;
  return output;
}

function HTMLDecode(text) { 
  var temp = document.createElement("div"); 
  temp.innerHTML = text; 
  var output = temp.innerText || temp.textContent; 
  temp = null; 
  return output; 
}

function copyText(text) {
  var textarea = document.createElement("textarea");
  textarea.style.opacity = 0;
  textarea.style.height = 0;
  var currentFocus = document.activeElement;
  var toolBoxwrap = document.querySelector("body"); 
  toolBoxwrap.appendChild(textarea);
  textarea.value = text;
  textarea.focus();
  if (textarea.setSelectionRange) {
      textarea.setSelectionRange(0, textarea.value.length);
  } else {
      textarea.select();
  }
  try {
      var flag = document.execCommand("copy");
  } catch (eo) {
      var flag = false;
  }
  toolBoxwrap.removeChild(textarea);
  currentFocus.focus();
  return flag;
}

let pres = document.querySelectorAll(".modeLoaded");
if (pres.length) {
  pres.forEach((item,index) => {
    let code = HTMLDecode(item.innerHTML.replace(/<pre\/?.+?>/g,"\n").replace(/<\/?.+?>/g,""));
    code = code.replace(/[\u0000-\u0007\u000B\u000E-\u001F\u200B]/g,"");
    code = code.trim();
    while(code.startsWith('x')){
      code = code.replace('x','');
    }
    code = code.trim();
    let btn = document.createElement("span");
    btn.className = "copybtn";
    btn.innerText = "copy";
    btn.onclick = ()=>{
        var flag = copyText(code);
        alert(flag ? '复制成功' : '复制失败');
    };
    item.insertBefore(btn,item.firstChild);
  });
}