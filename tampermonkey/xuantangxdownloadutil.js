// ==UserScript==
// @name         学堂在线视频下载辅助工具
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://ws.cdn.xuetangx.com/download/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var url = window.location.href;
    function getParam() {
        var res = {};
        var start = url.indexOf("?");
        if (start !== -1) {
            var str = url.substr(start);
            var strs = str.split("&");
            for(var i = 0; i < strs.length; i ++) {
                var item = strs[i].split("=");
                res[item[0]]=decodeURI(item[1]);
            }
        }
        return res;
    }
    var src = url.replace("download/","");
    var eleLink = document.createElement('a');
    console.log(url, getParam().name);
    eleLink.download = getParam().name+".mp4";
    eleLink.style.display = 'none';
    eleLink.href = src;
    document.body.appendChild(eleLink);
    eleLink.click();
    document.body.removeChild(eleLink);
    document.body.innerHTML="";
    
    // Your code here...
})();