### 一、CSS 历史与现状

`CSS` 是由 `W3C(万维网联盟)` 中的一个名叫 `CSS Working Group` 团体发展起来的。

`CSS` 被分为不同等级：`CSS1` 现已废弃， `CSS2.1` 是推荐标准，`CSS3` 分成多个小模块且正在标准化中。

### 二、CSS 属性继承

#### 1. 默认继承

https://developer.mozilla.org/zh-CN/docs/Web/CSS/:active

http://css.cuishifeng.cn/position.html

#### 2. 控制继承

inherit

initial

unset

### 三、CSS 选择器

#### 1. 选择器列表

- 标签选择器

- 类选择器

- ID选择器

- 通配选择器

- 属性选择器

- 状态选择器

- 组选择器

- 关系选择器
  - 邻近兄弟选择器
  - 兄弟选择器
  - 直接子代选择器
  - 后代元素选择器

- 伪类

- 伪元素

#### 2. 选择器优先级

1. 作者样式表与用户样式表（由低到高）
   - 用户代理样式表中的声明(例如，浏览器的默认样式，在没有设置其他样式时使用)。
   - 用户样式表中的常规声明(由用户设置的自定义样式)。
   - 作者样式表中的常规声明(这些是我们web开发人员设置的样式)。
   - 作者样式表中的`!important`声明
   - 用户样式表中的`!important` 声明

2. 优先级计算公式

   - 最高位： 内联样式该位得一分。这样的声明没有选择器，所以它得分总是1000。

   - 次高位： 选择器中包含ID选择器则该位得一分。

   - 次低位： 选择器中包含类选择器、属性选择器或者伪类则该位得一分。

   - 最低位：选择器中包含元素、伪元素选择器则该位得一分。

   - 通用选择器 (`*`)，组合符 (`+`, `>`, `~`, ' ')，和否定伪类 (`:not`) 不会影响优先级。

   - eg.

     | 选择器                                    | 千位 | 百位 | 十位 | 个位 | 优先级 |
     | :---------------------------------------- | :--- | :--- | :--- | :--- | :----- |
     | `h1`                                      | 0    | 0    | 0    | 1    | 0001   |
     | `h1 + p::first-letter`                    | 0    | 0    | 0    | 3    | 0003   |
     | `li > a[href*="en-US"] > .inline-warning` | 0    | 0    | 2    | 2    | 0022   |
     | `#identifier`                             | 0    | 1    | 0    | 0    | 0100   |
     | 内联样式                                  | 1    | 0    | 0    | 0    | 1000   |

3. 总结

   !important优先级最高，其余指向更明确的优先级更高

#### 4. 属性选择器详细用法

1. 属性有无匹配

   `[attr]`  eg. `a[title]`

2. 属性取值匹配

   `[attr=value]`  eg. `a[href="https://example.com"]`

3. 属性值取其一匹配

   `[attr~=value]`  eg. `a[class="container"]`

4. 属性值取前值匹配（需以连字符做后续）

   `[attr|=value]`  eg. `a[lang|="zh"]`

5. 属性值取前段匹配（需以连字符做后续）

   `[attr^=value]`  eg. `a[class^="title"]`

6. 属性值取后段匹配（需以连字符做后续）

   `[attr$=value]`  eg. `a[href$=".pdf"]`

7. 属性值取包含匹配（需以连字符做后续）

   `[attr*=value]`  eg. `a[class*="fire"]`

8. 大小写敏感

   如果你想在大小写不敏感的情况下，匹配属性值的话，你可以在闭合括号之前，使用`i`值。

### 四、css函数和@运算符

1. 函数
   - calc()
   - rotate()
   - var() 兼容性
   - attr()
   - rgb()、rgba()
   - hsl()、hsla()

2. @运算符
   - @import
   - @media

> notice
>
> 当浏览器遇到无法解析的CSS代码会发生什么?
>
> 答案就是浏览器什么也不会做，继续解析下一个CSS样式！
>
> 被组合起来的选择器，整个规则都会失效，所有选择器都不会被样式化。

### 五、CSS 盒模型

块级盒子（Block box） 和 内联盒子（Inline box）

一个被定义成块级的（block）盒子会表现出以下行为:

- 盒子会在内联的方向上扩展并占据父容器在该方向上的所有可用空间，在绝大数情况下意味着盒子会和父容器一样宽
- 每个盒子都会换行
- [`width`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/width) 和 [`height`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/height) 属性可以发挥作用
- 内边距（padding）, 外边距（margin） 和 边框（border） 会将其他元素从当前盒子周围“推开”

除非特殊指定，诸如标题(``等)和段落(``)默认情况下都是块级的盒子。

如果一个盒子对外显示为 `inline`，那么他的行为如下:

- 盒子不会产生换行。

-  [`width`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/width) 和 [`height`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/height) 属性将不起作用。

- 垂直方向的内边距、外边距以及边框会被应用但是不会把其他处于 `inline` 状态的盒子推开。

- 水平方向的内边距、外边距以及边框会被应用而且也会把其他处于 `inline` 状态的盒子推开。

  #### **外边距折叠**

  理解外边距的一个关键是外边距折叠的概念。如果你有两个外边距相接的元素，这些外边距将合并为一个外边距，即最大的单个外边距的大小。

  在下面的示例中，我们在一个段落中使用了``，并对其应用了宽度、高度、边距、边框和内边距。可以看到，宽度和高度被忽略了。外边距、内边距和边框是生效的，但它们不会改变其他内容与内联盒子的关系，因此内边距和边框会与段落中的其他单词重叠。

## 背景与边框

<div style="width:100px;height:100px;background-image: linear-gradient(105deg, rgba(0,249,255,1) 39%, rgba(51,56,57,1) 96%);"></div><div style="width:100px;height:100px;background-image: linear-gradient(90deg, rgba(0,249,255,1) 39%, rgba(51,56,57,1) 96%);"></div>

## 书写模式

我们在前面两节中学习了CSS的盒模型和CSS边框。在外边距、边框和内边距属性中，你会发现许多物理属性，例如 [`margin-top`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/margin-top)、 [`padding-left`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/padding-left)和 [`border-bottom`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-bottom)。就像width和height有映射，这些属性也有相应的映射。

`margin-top`属性的映射是[`margin-block-start`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/margin-block-start)——总是指向块级维度开始处的边距。

[`padding-left`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/padding-left)属性映射到 [`padding-inline-start`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/padding-inline-start)，这是应用到内联开始方向（这是该书写模式文本开始的地方）上的内边距。[`border-bottom`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-bottom)属性映射到的是[`border-block-end`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-block-end)，也就是块级维度结尾处的边框。

## 视口单位

[`object-fit`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/object-fit)

https://developer.mozilla.org/zh-CN/docs/Learn/CSS/CSS_layout