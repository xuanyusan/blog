## 媒体嵌入

### 一、图片

#### 1. alt属性的兼容性

```html
<img src="../../asset/imgs/headerB0.jpg" alt="崩坏3"/>
```

Safari：不会因为加载图片失败而改变图片占位大小来使得alt显示。

Chrome：会因为加载图片失败而改变图片占位大小来使得alt显示。

#### 2. HTML5中为图片配说明文字

```html
<figure>
  <img src="../../asset/imgs/headerBg0.jpg" alt="崩坏3" width="192" height="108"/>
  <figcaption>崩坏3</figcaption>
</figure>
```

### 二、视频

```html
<video src="rabbit320.webm" controls>
  <p>你的浏览器不支持 HTML5 视频。可点击<a href="rabbit320.mp4">此链接</a>观看</p>
</video>
```

#### 1. webm

> WebM由Google提出，是一个开放、免费的媒体文件格式。 WebM标准的网络视频更加偏向于开源并且是基于HTML5标准的

1. [在线转换](https://convertio.co/zh/mp4-webm/)

2. 与其它编码格式的对比

   | 容器 | 内容                                  | 兼容性                                             |
   | ---- | ------------------------------------- | -------------------------------------------------- |
   | WebM | Opus <br>Vorbis 音频 <br>VP8/VP9 视频 | 所有的现代浏览器中都支持，除了他们的老版本         |
   | MP4  | AAC <br>MP3 音频 <br>H.264 视频       | 所有的现代浏览器中都支持，还有 Internet Explorer。 |
   | Ogg  | Ogg Vorbis  音频<br> Ogg Theora 视频  | 在 Firefox 和 Chrome 当中支持                      |

3. source的使用

   ```html
   <video controls>
     <source src="../../asset/videos/1.mp4" type="video/mp4">
     <source src="../../asset/videos/1.webm" type="video/webm">
     <p>你的浏览器不支持 HTML5 视频。可点击<a href="../../asset/videos/1.mp4">此链接</a>观看</p>
   </video>
   ```

   `source` 含有一个 `type` 属性，建议添加。

   浏览器也会通过检查这个属性来迅速的跳过那些不支持的格式。

4. 其它属性

   - poster 添加封面
   - preload 缓冲

5. 音频的一些概念

   audio 音轨增删事件

6. 字幕

   ```html
   <video controls>
     <source src="../../asset/videos/1.mp4" type="video/mp4">
     <source src="../../asset/videos/1.webm" type="video/webm">
     <track kind="subtitles" src="../../asset/videos/1.vtt" srclang="zh">
     <p>你的浏览器不支持 HTML5 视频。可点击<a href="../../asset/videos/1.mp4">此链接</a>观看</p>
   </video>
   ```


### 三、iframe

1. 提供ifame代码的网站

   - bilibili

     <iframe src="//player.bilibili.com/player.html?aid=36570707&bvid=BV17t411y7ZG&cid=74641034&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true" height="300"> </iframe>

   - 百度地图

     <iframe>
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
     <html xmlns="http://www.w3.org/1999/xhtml">
     <head>
     <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
     <meta name="keywords" content="百度地图,百度地图API，百度地图自定义工具，百度地图所见即所得工具" />
     <meta name="description" content="百度地图API自定义地图，帮助用户在可视化操作下生成百度地图" />
     <title>百度地图API自定义地图</title>
     <!--引用百度地图API-->
     <style type="text/css">
         html,body{margin:0;padding:0;}
         .iw_poi_title {color:#CC5522;font-size:14px;font-weight:bold;overflow:hidden;padding-right:13px;white-space:nowrap}
         .iw_poi_content {font:12px arial,sans-serif;overflow:visible;padding-top:4px;white-space:-moz-pre-wrap;word-wrap:break-word}
     </style>
     <script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.1&services=true"></script>
     </head>
     
     <body>
       <!--百度地图容器-->
       <div style="width:697px;height:550px;border:#ccc solid 1px;" id="dituContent"></div>
     </body>
     <script type="text/javascript">
         //创建和初始化地图函数：
         function initMap(){
             createMap();//创建地图
             setMapEvent();//设置地图事件
             addMapControl();//向地图添加控件
         }
         
         //创建地图函数：
         function createMap(){
             var map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
             var point = new BMap.Point(116.395645,39.929986);//定义一个中心点坐标
             map.centerAndZoom(point,12);//设定地图的中心点和坐标并将地图显示在地图容器中
             window.map = map;//将map变量存储在全局
         }
         
         //地图事件设置函数：
         function setMapEvent(){
             map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
             map.enableScrollWheelZoom();//启用地图滚轮放大缩小
             map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
             map.enableKeyboard();//启用键盘上下左右键移动地图
         }
         
         //地图控件添加函数：
         function addMapControl(){
             //向地图中添加缩放控件
     	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
     	map.addControl(ctrl_nav);
             //向地图中添加缩略图控件
     	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
     	map.addControl(ctrl_ove);
             //向地图中添加比例尺控件
     	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
     	map.addControl(ctrl_sca);
         }
         
         
         initMap();//创建和初始化地图
     </script>
     </html></iframe>

2. 安全问题

   攻击手段: 单击劫持

   <iframe src="https://developer.mozilla.org/zh-CN/docs/Learn/HTML/Multimedia_and_embedding/其他嵌入技术"></iframe>

   防御手段: X-Frame-Options、sandbox属性

### 四、embed、object

1. embed插入flash动画的例子

   

2. object插入pdf的例子

### 五、svg

除非 SVG 和您当前的网页具有相同的 origin，否则你不能在主页面上使用 JavaScript 来操纵 SVG。

### 六、响应式



<picture>
  <source media="(max-width: 799px)" srcset="../../asset/imgs/headerBg0.jpg">
  <source media="(min-width: 800px)" srcset="../../asset/imgs/headerBg1.jpg">
  <img src="../../asset/imgs/headerBg0.jpg" alt="Chris standing up holding his daughter Elva">
</picture>

```html
<img srcset="elva-fairy-320w.jpg 320w,
             elva-fairy-480w.jpg 480w,
             elva-fairy-800w.jpg 800w"
     sizes="(max-width: 320px) 280px,
            (max-width: 480px) 440px,
            800px"
     src="elva-fairy-800w.jpg" alt="Elva dressed as a fairy">
```

如果你支持多种分辨率显示，但希望每个人在屏幕上看到的图片的实际尺寸是相同的，你可以让浏览器通过`srcset`和x语法结合——一种更简单的语法

```html
<img srcset="elva-fairy-320w.jpg,
             elva-fairy-480w.jpg 1.5x,
             elva-fairy-640w.jpg 2x"
     src="elva-fairy-640w.jpg" alt="Elva dressed as a fairy">
```

```html
<picture>
  <source media="(max-width: 799px)" srcset="elva-480w-close-portrait.jpg">
  <source media="(min-width: 800px)" srcset="elva-800w.jpg">
  <img src="elva-800w.jpg" alt="Chris standing up holding his daughter Elva">
</picture>
```