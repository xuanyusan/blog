### 一、块级元素和内联元素以及HTML5重新定义的类别

#### 1. 块级元素和内联元素

> 块级元素在页面中以占满行的块形式展现。块级元素通常用于展示页面上结构化的内容。一个以block形式展现的块级元素不会被嵌套进内联元素中，但可以嵌套在其它块级元素中。
>
> 内联元素通常出现在块级元素中并环绕文档内容的一小部分，而不是一整个段落或者一组内容。内联元素不会导致文本换行(br是一个例外)，它通常出现在一堆文字之间。

1. 内联元素(inline) 参照表[Block-level elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements)
   - 表单: {label,input, textarea}
   - 文本级别: {a, abbr, span, small, em, strong, ins, del, font, big, i, b, u, s, 引用[cite, q], 注音(ruby rb rp rt), time, 其它短语标签[dnf, code, samp, kbd, var], sub, sup, mark, bdo, bdi}
   - 回车: {br, wbr}
   - 媒体嵌入: {img, audio, video, canvas}

2. 块级元素(block)  参照表[Inline elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Inline_elements).
   - address, blockquote, form, h1–h6, div, dl, ol, ul, li, p, pre, table, dir, fieldset, hr, menu

#### 2. HTML5元素标签分类

1. 分类内容

   - [Metadata content](https://html.spec.whatwg.org/multipage/dom.html#metadata-content-2)
   - [Flow content](https://html.spec.whatwg.org/multipage/dom.html#flow-content-2)
   - [Sectioning content](https://html.spec.whatwg.org/multipage/dom.html#sectioning-content-2)
   - [Heading content](https://html.spec.whatwg.org/multipage/dom.html#heading-content-2)
   - [Phrasing content](https://html.spec.whatwg.org/multipage/dom.html#phrasing-content-2)
   - [Embedded content](https://html.spec.whatwg.org/multipage/dom.html#embedded-content-category)
   - [Interactive content](https://html.spec.whatwg.org/multipage/dom.html#interactive-content-2)

2. 分类表单

   | Category                                                     | Elements                                                     | Elements with exceptions                                     |
   | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
   | 主根                                                         | `html`                                                       |                                                              |
   | 元数据                                                       | `base`; `link`; `meta`; `noscript`; `script`; `style`; `template`; `title` | —                                                            |
   | [Flow content](https://html.spec.whatwg.org/multipage/dom.html#flow-content-2) | `a`; `abbr`; `address`; `article`; `aside`; `audio`; `b`; `bdi`; `bdo`; `blockquote`; `br`; `button`; `canvas`; `cite`; `code`; `data`; `datalist`; `del`; `details`; `dfn`; `dialog`; `div`; `dl`; `em`; `embed`; `fieldset`; `figure`; `footer`; `form`; `h1`; `h2`; `h3`; `h4`; `h5`; `h6`; `header`; `hgroup`; `hr`; `i`; `iframe`; `img`; `input`; `ins`; `kbd`; `label`; `map`; `mark`; [MathML `math`](https://www.w3.org/Math/draft-spec/chapter2.html#interf.toplevel); `menu`; `meter`; `nav`; `noscript`; `object`; `ol`; `output`; `p`; `picture`; `pre`; `progress`; `q`; `ruby`; `s`; `samp`; `script`; `section`; `select`; `slot`; `small`; `span`; `strong`; `sub`; `sup`; [SVG `svg`](https://svgwg.org/svg2-draft/struct.html#SVGElement); `table`; `template`; `textarea`; `time`; `u`; `ul`; `var`; `video`; `wbr`; [autonomous custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#autonomous-custom-element); [Text](https://html.spec.whatwg.org/multipage/dom.html#text-content) | `area` (if it is a descendant of a `map` element); `link` (if it is [allowed in the body](https://html.spec.whatwg.org/multipage/semantics.html#allowed-in-the-body)); `main` (if it is a [hierarchically correct `main` element](https://html.spec.whatwg.org/multipage/grouping-content.html#hierarchically-correct-main-element)); `meta` (if the `itemprop` attribute is present) |
   | 分区                                                         | `article`; `aside`; `nav`; `section`                         | —                                                            |
   | 标题                                                         | `h1`; `h2`; `h3`; `h4`; `h5`; `h6`; `hgroup`                 | —                                                            |
   | 文本内容                                                     | `a`; `abbr`; `audio`; `b`; `bdi`; `bdo`; `br`; `button`; `canvas`; `cite`; `code`; `data`; `datalist`; `del`; `dfn`; `em`; `embed`; `i`; `iframe`; `img`; `input`; `ins`; `kbd`; `label`; `map`; `mark`; [MathML `math`](https://www.w3.org/Math/draft-spec/chapter2.html#interf.toplevel); `meter`; `noscript`; `object`; `output`; `picture`; `progress`; `q`; `ruby`; `s`; `samp`; `script`; `select`; `slot`; `small`; `span`; `strong`; `sub`; `sup`; [SVG `svg`](https://svgwg.org/svg2-draft/struct.html#SVGElement); `template`; `textarea`; `time`; `u`; `var`; `video`; `wbr`; [autonomous custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#autonomous-custom-element); [Text](https://html.spec.whatwg.org/multipage/dom.html#text-content) | `area` (if it is a descendant of a `map` element); `link` (if it is [allowed in the body](https://html.spec.whatwg.org/multipage/semantics.html#allowed-in-the-body)); `meta` (if the `itemprop` attribute is present) |
   | 嵌入式                                                       | `audio`; `canvas`; `embed`; `iframe`; `img`; [MathML `math`](https://www.w3.org/Math/draft-spec/chapter2.html#interf.toplevel); `object`; `picture`; [SVG `svg`](https://svgwg.org/svg2-draft/struct.html#SVGElement); `video` | —                                                            |
   | 交互式                                                       | `button`; `details`; `embed`; `iframe`; `label`; `select`; `textarea` | `a` (if the `href` attribute is present); `audio` (if the `controls` attribute is present); `img` (if the `usemap` attribute is present); `input` (if the `type` attribute is *not* in the [Hidden](https://html.spec.whatwg.org/multipage/input.html#hidden-state-(type=hidden)) state); `object` (if the `usemap` attribute is present); `video` (if the `controls` attribute is present) |
   | 分区根                                                       | `blockquote`; `body`; `details`; `dialog`; `fieldset`; `figure`; `td` | —                                                            |
   | 表单相关                                                     | `button`; `fieldset`; `input`; `label`; `object`; `output`; `select`; `textarea`; `img`; [form-associated custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#form-associated-custom-element) | —                                                            |
   | [Listed elements](https://html.spec.whatwg.org/multipage/forms.html#category-listed) | `button`; `fieldset`; `input`; `object`; `output`; `select`; `textarea`; [form-associated custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#form-associated-custom-element) | —                                                            |
   | 表单可提交                                                   | `button`; `input`; `object`; `select`; `textarea`; [form-associated custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#form-associated-custom-element) | —                                                            |
   | 可重置                                                       | `input`; `output`; `select`; `textarea`; [form-associated custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#form-associated-custom-element) | —                                                            |
   | [Autocapitalize-inheriting elements](https://html.spec.whatwg.org/multipage/forms.html#category-autocapitalize) | `button`; `fieldset`; `input`; `output`; `select`; `textarea` | —                                                            |
   | [Labelable elements](https://html.spec.whatwg.org/multipage/forms.html#category-label) | `button`; `input`; `meter`; `output`; `progress`; `select`; `textarea`; [form-associated custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#form-associated-custom-element) | —                                                            |
   | [Palpable content](https://html.spec.whatwg.org/multipage/dom.html#palpable-content-2) | `a`; `abbr`; `address`; `article`; `aside`; `b`; `bdi`; `bdo`; `blockquote`; `button`; `canvas`; `cite`; `code`; `data`; `details`; `dfn`; `div`; `em`; `embed`; `fieldset`; `figure`; `footer`; `form`; `h1`; `h2`; `h3`; `h4`; `h5`; `h6`; `header`; `hgroup`; `i`; `iframe`; `img`; `ins`; `kbd`; `label`; `main`; `map`; `mark`; [MathML `math`](https://www.w3.org/Math/draft-spec/chapter2.html#interf.toplevel); `meter`; `nav`; `object`; `output`; `p`; `pre`; `progress`; `q`; `ruby`; `s`; `samp`; `section`; `select`; `small`; `span`; `strong`; `sub`; `sup`; [SVG `svg`](https://svgwg.org/svg2-draft/struct.html#SVGElement); `table`; `textarea`; `time`; `u`; `var`; `video`; [autonomous custom elements](https://html.spec.whatwg.org/multipage/custom-elements.html#autonomous-custom-element) | `audio` (if the `controls` attribute is present); `dl` (if the element's children include at least one name-value group); `input` (if the `type` attribute is *not* in the [Hidden](https://html.spec.whatwg.org/multipage/input.html#hidden-state-(type=hidden)) state); `menu` (if the element's children include at least one `li` element); `ol` (if the element's children include at least one `li` element); `ul` (if the element's children include at least one `li` element); [Text](https://html.spec.whatwg.org/multipage/dom.html#text-content) that is not [inter-element whitespace](https://html.spec.whatwg.org/multipage/dom.html#inter-element-whitespace) |
   | 脚本                                                         | `script`; `template`                                         | —                                                            |

### 二、HTML特殊字符的转换

```javascript
function HTMLEncode(html) {
  var temp = document.createElement("div");
  (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
  var output = temp.innerHTML;
  temp = null;
  return output;
}

function HTMLDecode(text) { 
  var temp = document.createElement("div"); 
  temp.innerHTML = text; 
  var output = temp.innerText || temp.textContent; 
  temp = null; 
  return output; 
}

// 简单的编码译码方法
// 出处: https://www.cnblogs.com/peng-zhang/p/9870266.html
```

### 三、header中的要点

> 包含一些页面的元数据(matadata)

#### 1. SEO Search Engine Optimization

- meta description keywords 
- 为文档设定主语言

#### 2. Robots协议

- meta robots
- robot.txt

#### 3. 向某些网站(如社交网站)提供可使用的特定信息

```html
<meta property="og:image" content="./asset/imgs/share.jpg">
<meta property="og:title" content="My Blog">
<meta property="og:description" content="这是我学习html、css、javascript等前端技术的博客网站">
<meta name="twitter:title" content="My Blog">
<meta name="twitter:description" content="这是我学习html、css、javascript等前端技术的博客网站">
```

国内的社交平台的话可能QQ比较支持OGP，微信似乎要使用它的接口才可以。

我在苹果产品的备忘录和QQ上做了尝试。效果如下：

<img src="../../asset/imgs/blogInsImg/1_1.jpg" alt="img"/>

#### 4. 对每个页面只使用一次h1

最好对每个页面只使用一次h1，不然使用屏幕阅读器时，其它h1会被忽略。

可以看到下面这个例子中，"Web 学习"消失了。

![image-20200921214958933](../../asset/imgs/blogInsImg/1_2.jpg)

### 四、表象元素

### 五、链接

#### 1. Download

download 属性来提供一个默认的保存文件名（译注：此属性仅适用于同源URL）

#### 2. 电子邮件链接

eg. 

```html
<a href="mailto:nowhere@mozilla.org?cc=name2@rapidtables.com&bcc=name3@rapidtables.com&subject=The%20subject%20of%20the%20email&body=The%20body%20of%20the%20email">send email</a>
```

### 六、有趣的element

#### 1. 引用、引文

1. q

    <q>东风不与周郎便，铜雀春深锁二乔</q>

2. cite

   <cite>人人网</cite>

3. blockquote

   <blockquote>ach element in HTML falls into zero or more categories that group elements with similar characteristics together. </blockquote>

#### 2. 缩略 abbr

#### 3. 标记联系方式 address

#### 4. 上标、下标 sub sup

#### 5. 计算机代码 code var kbd

1. pre

   用于保留空白字符（通常用于代码块）

#### 6. 标记时间与日期

1. time

   <time datetime="2016-01-20">2016年1月20日</time>

#### 7. [所有的element](https://developer.mozilla.org/zh-CN/docs/Web/HTML/Element)

### 七、HTML校验

#### 1. [实用的校验网站](https://validator.w3.org/)